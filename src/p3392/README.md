# 3392 - Factory of Painted Wooden Toys

## Description

The International Consortium for Protection of Children’s (ICPC) has a big toy factory.
Toys are built from several materials such as iron, aluminum, wood, plastic, glass, etc.
Last month, the factory created a new toy that is well-liked by many children.
These toys consist of a wooden cube with side length equal to N centimeters.
The surface is completely painted with multiple random colors such that the toy has a stylish color combination

[comment]: # (TODO: Add image)

Furthermore, the wooden toys are cut to little cubic pieces of one centimeter on each side
(volume of each little cube is 1 cubic centimeter).
For instance,
a toy of side 3 centimeters long would be cut into exactly 27 little cubes.
Your task is to find how many of these little cubes have exactly two painted faces.

## Input specification

Input consists of several test cases, but no more than 1000.
Each case consists of a line with an integer number
N (1 <= N <= 5*10^3) representing the side length in centimeters of the toy.
The last line of input is followed by a line containing a zero, which should not be processed.

## Output specification

For each case, output a line with an integer
representing the number of little cubes that have exactly
two painted faces cut from the initial cube.

## Sample input

```
1
3
0
```

## Sample output

```
0
12
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[3376](http://coj.uci.cu/24h/problem.xhtml?pid=3376) |
[2769](http://coj.uci.cu/24h/problem.xhtml?pid=2769) |
[2441](http://coj.uci.cu/24h/problem.xhtml?pid=2441) |
[2534](http://coj.uci.cu/24h/problem.xhtml?pid=2534) |
[2141](http://coj.uci.cu/24h/problem.xhtml?pid=2141) |
[3232](http://coj.uci.cu/24h/problem.xhtml?pid=3232)

## Source

[Factory of Painted Wooden Toys](http://coj.uci.cu/24h/problem.xhtml?pid=3392)
