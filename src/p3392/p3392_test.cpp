/* Copyright (c) 2018 Kevin Villalobos */
#include "p3392.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = factory_of_painted_wooden_toys;

BOOST_AUTO_TEST_CASE(coj_1,
    *utf::timeout(1)) {
    const int result = tools::cubes(1);

    BOOST_REQUIRE_EQUAL(0, result);
}

BOOST_AUTO_TEST_CASE(coj_3,
    *utf::timeout(1)) {
    const int result = tools::cubes(3);

    BOOST_REQUIRE_EQUAL(12, result);
}

BOOST_AUTO_TEST_CASE(biggest,
    *utf::timeout(1)) {
    const int result = tools::cubes(5*(10e3));

    BOOST_REQUIRE_EQUAL(599976, result);
}

BOOST_AUTO_TEST_CASE(many_big,
    *utf::timeout(1)) {
    int max_tests = 1000;

    while (max_tests--) {
        const int result = tools::cubes(5*(10e3));

        BOOST_REQUIRE_EQUAL(599976, result);
    }
}
