/* Copyright (c) 2018 Kevin Villalobos */
#include "p3327.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = how_many_numbers;

BOOST_AUTO_TEST_CASE(coj_1_1,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        90,
        tools::eo_numbers(1, 1));
}

BOOST_AUTO_TEST_CASE(coj_2_0,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        40,
        tools::eo_numbers(2, 0));
}

BOOST_AUTO_TEST_CASE(coj_0_2,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        50,
        tools::eo_numbers(0, 2));
}

BOOST_AUTO_TEST_CASE(test_3_2,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        55000,
        tools::eo_numbers(3, 2));
}

BOOST_AUTO_TEST_CASE(all_even,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        6103515625000,
        tools::eo_numbers(18, 0));
}

BOOST_AUTO_TEST_CASE(all_odd,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        7629394531250,
        tools::eo_numbers(0, 18));
}

BOOST_AUTO_TEST_CASE(half_and_half,
    *utf::timeout(1)) {
    BOOST_REQUIRE_EQUAL(
        333847045898437500,
        tools::eo_numbers(9, 9));
}

BOOST_AUTO_TEST_CASE(max_big_tests,
    *utf::timeout(1)) {
    int max_tests = 200;

    while (max_tests--) {
        BOOST_REQUIRE_EQUAL(
            333847045898437500,
            tools::eo_numbers(9, 9));
    }
}
