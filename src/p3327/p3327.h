/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3327_P3327_H_
#define SRC_P3327_P3327_H_

#include <cstdint>

namespace how_many_numbers {

std::int64_t eo_numbers(const int &e, const int &o);

}

#endif  // SRC_P3327_P3327_H_
