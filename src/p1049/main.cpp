/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include "p1049.h"

namespace tools = sum;

int main() {
    int n;
    std::cin >> n;

    std::cout << sum::triangular_ish(n) << "\n";

    return 0;
}
