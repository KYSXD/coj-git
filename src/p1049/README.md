# 1049 - Sum

## Description

Your task is to find the sum of all integer numbers lying
between 1 and **N** inclusive.

## Input specification

The input consists of a single integer **N**
that is not greater than 10^4 by it's absolute value.

## Output specification

Write to the output a single integer number that is
the sum of all integer numbers lying between 1 and **N** inclusive.

## Sample input

```
3
```

## Sample output

```
6
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[1000](http://coj.uci.cu/24h/problem.xhtml?pid=1000) |
[1494](http://coj.uci.cu/24h/problem.xhtml?pid=1494) |
[1028](http://coj.uci.cu/24h/problem.xhtml?pid=1028) |
[3599](http://coj.uci.cu/24h/problem.xhtml?pid=3599) |
[1960](http://coj.uci.cu/24h/problem.xhtml?pid=1960) |
[3147](http://coj.uci.cu/24h/problem.xhtml?pid=3147)

## Source

[Sum](http://coj.uci.cu/24h/problem.xhtml?pid=1049)
