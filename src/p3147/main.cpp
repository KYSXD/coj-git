/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include <vector>
#include "p3147.h"

namespace tools = sum_challenge;

int main() {
    int t;
    std::cin >> t;

    for (int i = 0; i < t; i++) {
        int n;
        std::cin >> n;

        std::vector<int> numbers;
        for (int j = 0; j < n; j++) {
            int k;
            std::cin >> k;

            numbers.push_back(k);
        }

        switch (tools::zero_sum_subset(numbers)) {
            case false:
                std::cout << "NO\n";
                break;
            case true:
                std::cout << "YES\n";
                break;
        }
    }

    return 0;
}
